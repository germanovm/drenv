# drenv

Creating a minimal environment for testing ODF DR.

## Setup

Add yourself to the libvirt group (required for minikube kvm2 driver)

## Prepare an env file

Example file:

```
profiles:
  - name: "dr1"
    extra_disks: 1
    disk_size: "50g"
    scripts:
      - "rook/start"
  - name: "dr2"
    extra_disks: 1
    disk_size: "50g"
    scripts:
      - "rook/start"
  - name: "hub"
scripts:
  - file: "rook/setup-mirror"
    args: ["dr1", "dr2"]
  - file: "rook/setup-mirror"
    args: ["dr2", "dr1"]
```

### Env file format

- `profiles`: list of profiles.
   - `name`: profile name.
   - `extra_disks`: Number of extra disks (default 0)
   - `disk_size`: Disk size string (default "50g")
  - `scripts`: Optional list of scripts to run during start. The script
     is invoked with the profile name as the first argument.
- `scripts`: Optional list of scripts to run after the profile scripts.
    - `file`: Script filename
    - `args`: Optional argument to the script

## Starting the clusters

    ./drenv start env.yml

## Stopping the clusters

    ./drvenv stop env.yml

## Deleting clusters and disks

    ./drenv delete env.yml

## Options:

- -v, --verbose: Show verbose logs

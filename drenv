#!/usr/bin/python3

import argparse
import concurrent.futures
import logging
import subprocess
import sys
import time

from collections import deque

import yaml


def qemu_img(name, *cmd):
    run(name, "qemu-img", *cmd)


def minikube(name, cmd, *args):
    run(name, "minikube", cmd, "--profile", name, *args)


def run(name, *cmd):
    p = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT)

    messages = deque(maxlen=10)

    for line in iter(p.stdout.readline, b""):
        msg = line.decode().rstrip()
        messages.append(msg)
        logging.debug("[%s] %s", name, msg)

    p.wait()
    if p.returncode != 0:
        last_messages = "\n".join("  " + m for m in messages)
        raise RuntimeError(
            f"[{name}] Command {cmd} failed rc={p.returncode}\n"
            "\n"
            "Last messages:\n"
            f"{last_messages}")


def run_script(script):
    start = time.monotonic()
    logging.info("Running script %s", script["file"])
    run("env", script["file"], *script["args"])
    logging.info("Script %s completed in %.2f seconds",
                 script["file"], time.monotonic() - start)


def run_profile_script(profile, script):
    start = time.monotonic()
    logging.info("Running cluster %s script %s", profile["name"], script)
    run(profile["name"], script, profile["name"])
    logging.info("Cluster %s script %s completed in %.2f seconds",
                 profile["name"], script, time.monotonic() - start)


def start_cluster(profile):
    start = time.monotonic()
    logging.info("Starting cluster %s", profile["name"])
    minikube(profile["name"], "start",
             "--driver", "kvm2",
             "--container-runtime", "containerd",
             "--memory", "4g",
             "--extra-disks", str(profile["extra_disks"]),
             "--disk-size", profile["disk_size"],
             # Use same network for rbd mirroring.
             "--network", "default")

    for script in profile["scripts"]:
        run_profile_script(profile, script)

    logging.info("Cluster %s started in %.2f seconds",
                 profile["name"], time.monotonic() - start)


def stop_cluster(profile):
    start = time.monotonic()
    logging.info("Stopping cluster %s", profile["name"])
    minikube(profile["name"], "stop")
    logging.info("Cluster %s stopped in %.2f seconds",
                 profile["name"], time.monotonic() - start)


def delete_cluster(profile):
    start = time.monotonic()
    logging.info("Deleting cluster %s", profile["name"])
    minikube(profile["name"], "delete")
    logging.info("Cluster %s deleted in %.2f seconds",
                 profile["name"], time.monotonic() - start)


def read_env(filename):
    logging.info("Using %s", filename)
    with open(filename) as f:
        env = yaml.safe_load(f)
    validate_env(env)
    return env


def validate_env(env):
    if "profiles" not in env:
        raise ValueError("Missing profiles")

    for profile in env["profiles"]:
        validate_profile(profile)

    for script in env.setdefault("scripts", []):
        validate_script(script)


def validate_profile(profile):
    if "name" not in profile:
        raise ValueError("Missing profile name")

    profile.setdefault("extra_disks", 0)
    profile.setdefault("disk_size", "50g")
    profile.setdefault("scripts", [])


def validate_script(script):
    if "file" not in script:
        raise ValueError(f"Missing script 'file': {script}")
    script.setdefault("args", [])


def execute(func, profiles):
    failed = False

    with concurrent.futures.ThreadPoolExecutor() as e:
        futures = {e.submit(func, p): p["name"] for p in profiles}
        for f in concurrent.futures.as_completed(futures):
            try:
                f.result()
            except Exception:
                logging.exception("Cluster %s failed", futures[f])
                failed = True

    if failed:
        sys.exit(1)


def cmd_start(env):
    execute(start_cluster, env["profiles"])
    for script in env["scripts"]:
        run_script(script)


def cmd_stop(env):
    execute(stop_cluster, env["profiles"])


def cmd_delete(env):
    execute(delete_cluster, env["profiles"])


if __name__ == "__main__":
    commands = [n[4:] for n in globals() if n.startswith("cmd_")]

    p = argparse.ArgumentParser()
    p.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Be move verbose")
    p.add_argument(
        "command",
        choices=commands,
        help="Command to run")
    p.add_argument(
        "filename",
        help="Environment filename")
    args = p.parse_args()

    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        format="%(asctime)s %(levelname)-7s %(message)s")

    env = read_env(args.filename)

    func = globals()["cmd_" + args.command]
    func(env)

#!/bin/sh -e

OLM_BASE_URL="https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.19.1"

profile=${1:?Usage: $0 PROFILE}

kubectl() {
    minikube kubectl --profile $profile -- "$@"
}

echo "Deploying olm crds"
kubectl apply -f $OLM_BASE_URL/crds.yaml

echo "Waiting until cdrs are established"
kubectl wait --for condition=established -f $OLM_BASE_URL/crds.yaml

echo "Deploying olm"
kubectl apply -f $OLM_BASE_URL/olm.yaml

echo "Waiting for olm operator rollout"
kubectl rollout status deployment/olm-operator \
    --namespace olm \
    --watch

echo "Waiting for olm catalog operator rollout"
kubectl rollout status deployment/catalog-operator \
    --namespace olm \
    --watch

echo "Waiting for olm pakcage server rollout"
kubectl rollout status deployment/packageserver \
    --namespace olm \
    --watch

echo "Waiting until olm pakcage server succeeds"
kubectl wait csv/packageserver \
    --namespace olm \
    --for 'jsonpath={.status.phase}=Succeeded' \
    --timeout 300s

echo "Deleting operatorhubio-catalog"
kubectl delete catalogsources.operators.coreos.com/operatorhubio-catalog \
    --namespace olm \
    --ignore-not-found
